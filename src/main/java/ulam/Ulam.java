/*
 * Copyright (c) 2012, Andreas Donig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package ulam;

import java.math.BigInteger;

import static java.math.BigInteger.ONE;

public class Ulam {
    private enum Color {
        WHITE, GRAY, BLACK;
    }

    private static final BigInteger TWO = BigInteger.valueOf(2L);
    private static final BigInteger THREE = BigInteger.valueOf(3L);
    private static final BigInteger FOUR = BigInteger.valueOf(4L);
    private static final BigInteger SIX = BigInteger.valueOf(6L);

    public static void main(String[] args) {
        int depth = 100;

        BigInteger[] value = new BigInteger[depth + 1];
        value[0] = ONE;

        Color[] color = new Color[depth + 1];
        for (int i = 0; i < color.length; ++i) {
            color[i] = Color.WHITE;
        }

        long[] nodesWithOneChildAtDepth = new long[depth + 1];
        long[] nodesWithTwoChildrenAtDepth = new long[depth + 1];

        int currentDepth = 0;
        do {
            if (currentDepth < depth) {
                if (color[currentDepth] == Color.WHITE) {
                    if (currentDepth > 3 && value[currentDepth].mod(SIX).equals(FOUR)) {
                        color[currentDepth] = Color.GRAY;
                        value[currentDepth + 1] = value[currentDepth].subtract(ONE).divide(THREE);
                        ++nodesWithTwoChildrenAtDepth[currentDepth];
                    } else {
                        color[currentDepth] = Color.BLACK;
                        value[currentDepth + 1] = value[currentDepth].multiply(TWO);
                        ++nodesWithOneChildAtDepth[currentDepth];
                    }
                    ++currentDepth;
                } else if (color[currentDepth] == Color.GRAY) {
                    color[currentDepth] = Color.BLACK;
                    value[currentDepth + 1] = value[currentDepth].multiply(TWO);
                    ++currentDepth;
                }
            } else {
                if (currentDepth > 3 && value[currentDepth].mod(SIX).equals(FOUR)) {
                    ++nodesWithTwoChildrenAtDepth[currentDepth];
                } else {
                    ++nodesWithOneChildAtDepth[currentDepth];
                }
                do {
                    color[currentDepth] = Color.WHITE;
                    --currentDepth;
                } while (currentDepth >= 0 && color[currentDepth] == Color.BLACK);
            }
        } while (currentDepth >= 0);

        for (int i = 0; i < depth + 1; ++i) {
            System.out.format("%d,%d\n", nodesWithOneChildAtDepth[i], nodesWithTwoChildrenAtDepth[i]);
        }
    }
}
